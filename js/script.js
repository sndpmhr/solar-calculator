app = angular.module('solarcal', ['ngMaterial', 'ngAria', 'ngMessages']);
// app.directive('dataTile', function(){
//  var directive = {};
//  directive.restrict = 'E';
//  directive.template = ' <md-grid-tile> <div layout-padding layout="row"> <div flex="100"> 500 kWs </div></div><md-grid-tile-footer><h3>Solar Energy</h3></md-grid-tile-footer></md-grid-tile>';
//   directive.compile = function(element, attributes) {
//       element.css("background", "#f5f5f5");

//       //linkFunction is linked with each element with scope to get the element specific data.
//       var linkFunction = function($scope, element, attributes) {
//          element.html('<md-grid-tile> <div layout-padding layout="row"> <div flex="100"> 500 kWs </div></div><md-grid-tile-footer><h3>Solar Energy</h3></md-grid-tile-footer></md-grid-tile>');
//       }
//       return linkFunction;
//    }
//    return directive;
// });

app.controller('MainCtrl', function($scope) {
    $scope.input = {};
    $scope.output = {}
    $scope.showInput = true;
    $scope.showOutput = false;
    $scope.calbtn = true;
    $scope.loanbtn = false;
    $scope.showLoanForm = false;
    $scope.showloanamount = false;
    $scope.input.tenure = 5;
    $scope.inputboxsize = 66;

    $scope.cities = [{
        'Name': 'Pune',
        'Tariff': 9
    }, {
        'Name': 'Bangalore',
        'Tariff': 5
    }, {
        'Name': 'Mumbai',
        'Tariff': 7
    }]

    // calculate solar size , solar units and solar cost
    $scope.calMax = function() {
            if ($scope.input.units && $scope.input.roofSize) {
                // $scope.showloanamount = true;
                $scope.output.sysSize = Math.round(Math.min($scope.input.units / 120, $scope.input.roofSize / 100) * 10) / 10;
                $scope.output.solarUnits = ($scope.output.sysSize * 4 * 30);
                $scope.output.sysCost = ($scope.output.sysSize * 75000);
                $scope.input.loan = $scope.output.sysCost * 0.5;
                $scope.calEMI();
                console.log($scope.output);
            }
        }
        // calculate EMI 
    $scope.calEMI = function() {
            if ($scope.input.units && $scope.input.roofSize && $scope.input.tenure && $scope.input.loan && $scope.input.city.Name) {
                $scope.output.EMI = Math.round(($scope.input.loan + ($scope.input.loan * 11 * $scope.input.tenure) / 100) / ($scope.input.tenure * 12));
                console.dir($scope.output);
                $scope.output.downpayment = $scope.output.sysCost - $scope.input.loan;
                $scope.data();
                $scope.calSaving();

            }
        }
        // saving on monthly electricity bill
    $scope.calSaving = function() {
            $scope.cBill = $scope.input.units * $scope.input.city.Tariff;
            $scope.rUnits = Math.max($scope.input.units - $scope.output.solarUnits, 0);
            $scope.rBill = $scope.rUnits * $scope.input.city.Tariff;
            $scope.output.saving = $scope.cBill - $scope.rBill - $scope.output.EMI;
        }
        // calculate solar specification on location change and first time calculation 
    $scope.calculate = function() {

        //$scope.input.loan = $scope.output.sysCost * 0.5;
        $scope.calbtn = false;
        $scope.showLoanForm = true;
        $scope.showloanamount = true;
        $scope.loanbtn = true;
        $scope.inputboxsize = 35;
        $scope.outputboxsize = 50;
        $scope.showOutput = true;

        $scope.calEMI();
    };
    $scope.getloan = function() {

    }


    // generate data and rendering chart
    $scope.data = function() {
        //$scope.cBill=1200;
        $scope.years = 10;
        $scope.noSolar = [];
        $scope.solarNoLoan = [];
        $scope.solarLoan = [];
        var j = 0;
        for (i = 0; i <= $scope.years; i++) {

            if (i <= $scope.input.tenure) {
                j = i;
            }

            $scope.noSolar.push($scope.cBill * 12 * i);
            $scope.solarNoLoan.push($scope.output.sysCost + $scope.rBill * 12 * i);
            $scope.solarLoan.push($scope.output.downpayment + $scope.rBill * 12 * i + $scope.output.EMI * 12 * j);
            //$scope.solarLoan.push({'X':i,'Y':})
        }

        Highcharts.chart('container', {
            title: {
                text: 'PayBack Grpah',
                x: -20 //center
            },

            xAxis: {
                title: {
                    text: 'Time(years)'
                },
                categories: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
            },
            yAxis: {
                title: {
                    text: 'cash outflow on electricity (Rs.)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                borderRadius    : 2,
                valuePrefix: 'Rs. ',
                borderWidth     : 1,
                shadow          : false,
                shared          : true,
                useHTML         : true
                
            },
            series: [{
                name: 'No Solar',
                data: $scope.noSolar
            }, {
                name: 'Solar without loan',
                data: $scope.solarNoLoan
            }, {
                name: 'Solar with loan',
                data: $scope.solarLoan
            }]
        });
    }




});